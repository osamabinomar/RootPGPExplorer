package com.slownet5.pgprootexplorer.utils;

import androidx.appcompat.app.AppCompatActivity;

import com.slownet5.pgprootexplorer.R;
/**
 *
 * Created by bullhead on 5/24/17.
 *
 */

public class MainUtils {
    public static void closeActionBarButton(AppCompatActivity act){
        assert act.getSupportActionBar()!=null;
        act.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_close);
        act.getSupportActionBar().setHomeButtonEnabled(true);

    }
}
